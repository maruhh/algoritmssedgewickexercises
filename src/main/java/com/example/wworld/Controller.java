package com.example.wworld;

import javax.annotation.Resource;

public class Controller {
    @Resource
    private Service service;


    public String personMessage(String name) {
//        Beckett.moves()
//        StdDraw.setCanvasSize(800, 800);
        return service.returnPersonMessageByName(name);
    }

}
