package com.example.wworld;

import org.springframework.data.jpa.repository.JpaRepository;

public abstract class Repository implements JpaRepository<Person, String> {

}
