package com.example.wworld;

import javax.annotation.Resource;

public class Service {
    @Resource
    private Repository repository;

    public String returnPersonMessageByName(String name) {
        Person person = repository.findOne(name);
        return person.getMessage();
    }
}
