package com.example.wworld._1_Fundamentals._1_1_BasicProgModel;

import com.example.wworld.StdLib.StdOut;

public class Ex_1_1_07 {

    public static void main(String[] args)
    {
        double t = 9.0;
        while (Math.abs(t - 9.0/t) > .00001)
        {
            t = (9.0/t + t) / 2.0;
            StdOut.printf("%.5f\n", t);
        }

        StdOut.println("Second task");

        int sum = 0;
        for (int i = 1; i < 1000; i++) {
            for (int j = 0; j < i; j++) {
                sum++;
            }
        }

        int sum2 = 0;
        for (int i = 0; i < 1000; i++) {
            sum2 = sum2 + i;
        }

        StdOut.println("Sum: " + sum);
        StdOut.println("Sum2: " + sum2);

    }

}
