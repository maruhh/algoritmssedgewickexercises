package com.example.wworld._1_Fundamentals._1_1_BasicProgModel;

import java.util.LinkedList;

import com.example.wworld.StdLib.StdOut;

public class Ex_1_1_19 {


    public static long Fib(int N) {
        long[] f = new long[N + 1];
        return Fib(N, f);
    }

    private static long Fib(int N, long[] f) {
        if (f[N] == 0) {
            if (N == 1)
                f[N] = 1;
            else if (N > 1)
                f[N] = Fib(N - 1, f) + Fib(N - 2, f);
        }

        return f[N];
    }


    private static LinkedList<Long> fib = new LinkedList<>();

    private static long fibonacci(int N) {
        if (N == 0) {
            fib.add(0L);
            return 0;
        }
        if (N == 1) {
            fib.add(1L);
            return 1;
        }

        long fibonacci = fib.get(N - 1) + fib.get(N - 2);
        fib.add(fibonacci);
        return fibonacci;
    }


    private static long F(int N) {
        if (N == 0) return 0;
        if (N == 1) return 1;
        return F(N - 1) + F(N - 2);
    }


    public static void main(String[] args) {
//        for (int N = 0; N < 100; N++)
//            StdOut.println(N + " " + F(N));
        for (int N = 0; N < 10000; N++)
            StdOut.println(N + " " + fibonacci(N));
    }

}
