package com.example.wworld._1_Fundamentals._1_1_BasicProgModel;

import com.example.wworld.StdLib.StdOut;

public class Ex_1_1_06 {

    public static void main(String[] args) {

        int f = 0;
        int g = 1;
        for (int i = 0; i <= 15; i++)
        {
            StdOut.println(f);
            f = f + g;
            g = f - g;
        }

    }

}
